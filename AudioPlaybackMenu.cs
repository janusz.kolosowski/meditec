﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Appium.Windows;
using System;

namespace VlcTest
{
    /// <summary>
    /// Test class for verifying menu controls for Playback menu
    /// </summary>
    [TestClass]
    public class AudioPlaybackMenu : VlcSession
    {
        private static WindowsElement PlaybackMenuButton
        {
            get
            {
                return vlcSession.FindElementByName("Playback Alt+l");
            }
        }
        private static WindowsElement PlayButton{
            get
            {
                return vlcSession.FindElementByName("Play Alt+P");
            }
        } 
        private static WindowsElement StopButton
        {
            get
            {
                return vlcSession.FindElementByName("Stop Alt+S");
            }
        } 
        private static WindowsElement PreviousButton
        {
            get
            {
                return vlcSession.FindElementByName("Previous Alt+v");
            }
        }
            
        private static WindowsElement NextButton
        {
            get
            {
                return vlcSession.FindElementByName("Next Alt+x");
            }
        } 
        private static WindowsElement MainWindow
        {
            get
            {
                return vlcSession.FindElementByClassName("Qt5QWindowIcon");
            }
        }
        private static WindowsElement PauseButton
        {
            get
            {
                return vlcSession.FindElementByName("Pause");
            }
        }

        /// <summary>
        /// Checks if playback of audio is enabled when an audio file is loaded
        /// </summary>
        [TestMethod]
        public void PlayButtonEnabled()
        {
            PlaybackMenuButton.Click();
            Assert.IsTrue(PlayButton.Enabled);
        }

        /// <summary>
        /// Checks if main window title changes when music playback is started
        /// </summary>
        [TestMethod]
        public void PlayButtonTitleChange()
        {
            String WindowTitleBefore = MainWindow.GetAttribute("Name");
            PlaybackMenuButton.Click();
            PlayButton.Click();
            String WindowTitleAfter = MainWindow.GetAttribute("Name");
            Assert.IsFalse(WindowTitleBefore.Contains(WindowTitleAfter));
        }

        /// <summary>
        /// Checks if play button in playback menu changes to Pause button when Play is pressed
        /// </summary>
        [TestMethod]
        public void PlayButtonChangeToPause()
        {
            PlaybackMenuButton.Click();
            PlayButton.Click();
            PlaybackMenuButton.Click();
            Assert.IsTrue(PauseButton.Enabled);
        }

        /// <summary>
        /// Checks if pause button in playback menu changes to Play button when Pause is pressed
        /// </summary>
        [TestMethod]
        public void PauseButtonChangeToPlay()
        {
            PlaybackMenuButton.Click();
            PlayButton.Click();
            PlaybackMenuButton.Click();
            Assert.IsTrue(PauseButton.Enabled);
            PauseButton.Click();
            PlaybackMenuButton.Click();
            Assert.IsTrue(PlayButton.Enabled);
        }

        /// <summary>
        /// Checks if main window title does not change when music playback is paused
        /// </summary>
        [TestMethod]
        public void PauseButtonTitleNoChange()
        {

            PlaybackMenuButton.Click();
            PlayButton.Click();
            String WindowTitleBefore = MainWindow.GetAttribute("Name");
            PlaybackMenuButton.Click();
            PauseButton.Click();
            String WindowTitleAfter = MainWindow.GetAttribute("Name");
            Assert.IsTrue(WindowTitleBefore.Contains(WindowTitleAfter));
        }

        /// <summary>
        /// Checks if stop button in playback menu is not enabled while audio playback is not enabled 
        /// </summary>
        [TestMethod]
        public void StopButtonIsDisabled()
        {
            PlaybackMenuButton.Click();
            Assert.IsFalse(StopButton.Enabled);
        }

        /// <summary>
        /// Checks if stop button in playback menu is enabled while audio playback is enabled 
        /// </summary>
        [TestMethod]
        public void StopButtonIsEnabledWhilePlaying()
        {
            PlaybackMenuButton.Click();
            PlayButton.Click();
            PlaybackMenuButton.Click();
            Assert.IsTrue(StopButton.Enabled);
        }

        /// <summary>
        /// Checks if stop button in playback menu is enabled while audio playback is paused 
        /// </summary>
        [TestMethod]
        public void StopButtonIsEnabledWhilePaused()
        {
            PlaybackMenuButton.Click();
            PlayButton.Click();
            PlaybackMenuButton.Click();
            PauseButton.Click();
            PlaybackMenuButton.Click();
            Assert.IsTrue(StopButton.Enabled);
        }

        /// <summary>
        /// Checks if stop button changes main window title after stopping audio playback 
        /// </summary>
        [TestMethod]
        public void StopButtonTitleChange()
        {
            PlaybackMenuButton.Click();
            PlayButton.Click();
            String WindowTitleBefore = MainWindow.GetAttribute("Name");
            PlaybackMenuButton.Click();
            StopButton.Click();
            String WindowTitleAfter = MainWindow.GetAttribute("Name");
            Assert.IsFalse(WindowTitleBefore.Contains(WindowTitleAfter));
        }

        /// <summary>
        /// Checks if Previous button is available when audio playback is stopped 
        /// </summary>
        [TestMethod]
        public void PreviousButtonIsEnabledWhenStopped()
        {
            PlaybackMenuButton.Click();
            Assert.IsTrue(PreviousButton.Enabled);
        }

        /// <summary>
        /// Checks if Previous button is available when audio playback is started 
        /// </summary>
        [TestMethod]
        public void PreviousButtonIsEnabledWhenPlaying()
        {
            PlaybackMenuButton.Click();
            PlayButton.Click();
            PlaybackMenuButton.Click();
            Assert.IsTrue(PreviousButton.Enabled);
        }

        /// <summary>
        /// Checks if Previous button is available when audio playback is paused 
        /// </summary>
        [TestMethod]
        public void PreviousButtonIsEnabledWhenPaused()
        {
            PlaybackMenuButton.Click();
            PlayButton.Click();
            PlaybackMenuButton.Click();
            PauseButton.Click();
            PlaybackMenuButton.Click();
            Assert.IsTrue(PreviousButton.Enabled);
        }

        /// <summary>
        /// Checks if Previous button causes main window title to change when pressed 
        /// </summary>
        [TestMethod]
        public void PreviousButtonMainWindowTitleChange()
        {
            String WindowTitleBefore = MainWindow.GetAttribute("Name");
            PlaybackMenuButton.Click();
            PreviousButton.Click();
            String WindowTitleAfter = MainWindow.GetAttribute("Name");
            Assert.IsFalse(WindowTitleAfter.Contains(WindowTitleBefore));
        }

        /// <summary>
        /// Checks if Previous button triggers audio playback (pause button enabled) 
        /// </summary>
        [TestMethod]
        public void PreviousButtonPauseButtonEnabled()
        {
            PlaybackMenuButton.Click();
            PreviousButton.Click();
            PlaybackMenuButton.Click();
            Assert.IsTrue(PauseButton.Enabled);
        }

        /// <summary>
        /// Checks if Next button is available when audio playback is stopped 
        /// </summary>
        [TestMethod]
        public void NextButtonIsEnabledWhenStopped()
        {
            PlaybackMenuButton.Click();
            Assert.IsTrue(NextButton.Enabled);
        }

        /// <summary>
        /// Checks if Next button is available when audio playback is started 
        /// </summary>
        [TestMethod]
        public void NextButtonIsEnabledWhenPlaying()
        {
            PlaybackMenuButton.Click();
            PlayButton.Click();
            PlaybackMenuButton.Click();
            Assert.IsTrue(NextButton.Enabled);
        }

        /// <summary>
        /// Checks if Next button is available when audio playback is paused 
        /// </summary>
        [TestMethod]
        public void NextButtonIsEnabledWhenPaused()
        {
            PlaybackMenuButton.Click();
            PlayButton.Click();
            PlaybackMenuButton.Click();
            PauseButton.Click();
            PlaybackMenuButton.Click();
            Assert.IsTrue(NextButton.Enabled);
        }

        /// <summary>
        /// Checks if Next button causes main window title to change when pressed 
        /// </summary>
        [TestMethod]
        public void NextButtonMainWindowTitleChange()
        {
            String WindowTitleBefore = MainWindow.GetAttribute("Name");
            PlaybackMenuButton.Click();
            NextButton.Click();
            String WindowTitleAfter = MainWindow.GetAttribute("Name");
            Assert.IsFalse(WindowTitleAfter.Contains(WindowTitleBefore));
        }

        /// <summary>
        /// Checks if Next button triggers audio playback (pause button enabled) 
        /// </summary>
        [TestMethod]
        public void NextButtonPauseButtonEnabled()
        {
            PlaybackMenuButton.Click();
            NextButton.Click();
            PlaybackMenuButton.Click();
            Assert.IsTrue(PauseButton.Enabled);
        }
        
        [ClassInitialize]
        public static void ClassInitialize()
        {
            // Open a folder with several music files before performing audio playback tests
            vlcSession.FindElementByName("Media Alt+M").Click();
            vlcSession.FindElementByName("Open Folder... Ctrl+F").Click();
            // Folder selection edit element can't be addressed by name (since it is not unique),
            // attempting to use "automation ID" instead
            vlcSession.FindElementById("1152").SendKeys(@"C:\Users\Public\Music\Sample Music");
            vlcSession.FindElementByName("Select Folder").Click();
            // Since the VLC player will immediately start playing music from selected folder, we stop playback here:
            PlaybackMenuButton.Click();
            StopButton.Click();
        }

        [ClassCleanup]
        public static void ClassCleanup()
        {
            // Restart VLC media player to make sure no audio files are prepared for playing
            // TODO
        }

        [TestCleanup]
        public static void TestCleanup()
        {
            // Stop any music playback initiated by tests in this class
            PlaybackMenuButton.Click();
            StopButton.Click();
        }
    }
}
