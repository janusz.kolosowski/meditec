﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Appium.Windows;
using System;

namespace VlcTest
{
    /// <summary>
    /// Test class for verifying media management operations
    /// </summary>
    [TestClass]
    public class VolumeControl : VlcSession
    {
        private static WindowsElement AudioMenuButton
        {
            get
            {
                return vlcSession.FindElementByName("Audio Alt+A");
            }
        }
        private static WindowsElement IncreaseVolumeButton
        {
            get
            {
                return vlcSession.FindElementByName("Increase Volume Alt+I");
            }
        }
        private static WindowsElement DecreaseVolumeButton
        {
            get
            {
                return vlcSession.FindElementByName("Decrease Volume Alt+e");
            }
        }
        private static WindowsElement MuteButton
        {
            get
            {
                return vlcSession.FindElementByName("Mute Alt+M");
            }
        }

        /// <summary>
        /// Checks if Increase Volume button is available by default
        /// </summary>
        [TestMethod]
        public void IncreaseVolumeIsAvailable()
        {
            AudioMenuButton.Click();
            Assert.IsTrue(IncreaseVolumeButton.Enabled);
        }

        /// <summary>
        /// Checks if Decrease Volume button is available by default
        /// </summary>
        [TestMethod]
        public void DecreaseVolumeIsAvailable()
        {
            AudioMenuButton.Click();
            Assert.IsTrue(DecreaseVolumeButton.Enabled);
        }

        /// <summary>
        /// Checks if Mute button is available by default
        /// </summary>
        [TestMethod]
        public void MuteIsAvailable()
        {
            AudioMenuButton.Click();
            Assert.IsTrue(MuteButton.Enabled);
        }

        [ClassInitialize]
        public static void ClassInitialize()
        {
            // Seems like no specific actions needed here
            // To be checked by running tests
        }

        [ClassCleanup]
        public static void ClassCleanup()
        {
            // Seems like no specific actions needed here
            // To be checked by running tests
        }

        [TestInitialize]
        public static void TestInitialize()
        {
            // Set volume to a known value
            // TODO - volume slider does not have a unique reference that can be used...
        }
    }
}
