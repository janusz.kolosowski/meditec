﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Appium.Windows;
using OpenQA.Selenium.Remote;
using System;

namespace VlcTest
{
    public class VlcSession
    {
        private const string WinAppDriverUrl = "http://127.0.0.1:4723";
        private const string VlcAppAddress = @"C:\Program Files (x86)\VideoLAN\VLC\vlc.exe";

        protected static WindowsDriver<WindowsElement> vlcSession;

        public static void Setup()
        {
            if (vlcSession == null)
            {
                DesiredCapabilities appCapabilities = new DesiredCapabilities();
                appCapabilities.SetCapability("app", VlcAppAddress);
                vlcSession = new WindowsDriver<WindowsElement>(new Uri(WinAppDriverUrl), appCapabilities);
                Assert.IsNotNull(vlcSession);
            }
        }

        public static void TearDown()
        {
            if (vlcSession != null)
            {
                vlcSession.Quit();
                vlcSession = null;
            }
        }

        [TestInitialize]
        public virtual void TestInit()
        {
            //To be tested
        }
    }
}
